-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server Version:               10.4.21-MariaDB - mariadb.org binary distribution
-- Server Betriebssystem:        Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Exportiere Datenbank Struktur für krautundrueben
CREATE DATABASE IF NOT EXISTS `krautundrueben` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `krautundrueben`;

-- Exportiere Struktur von Tabelle krautundrueben.allergene
CREATE TABLE IF NOT EXISTS `allergene` (
  `ALLERGIEID` int(11) NOT NULL,
  `ALLLERGIENAME` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ALLERGIEID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle krautundrueben.allergene: ~6 rows (ungefähr)
DELETE FROM `allergene`;
/*!40000 ALTER TABLE `allergene` DISABLE KEYS */;
INSERT INTO `allergene` (`ALLERGIEID`, `ALLLERGIENAME`) VALUES
	(1, 'Lactose'),
	(2, 'Nuss'),
	(3, 'Weizen'),
	(4, 'Apfel'),
	(5, 'ei'),
	(6, 'Fisch');
/*!40000 ALTER TABLE `allergene` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle krautundrueben.bestellung
CREATE TABLE IF NOT EXISTS `bestellung` (
  `BESTELLNR` int(11) NOT NULL AUTO_INCREMENT,
  `KUNDENNR` int(11) DEFAULT NULL,
  `BESTELLDATUM` date DEFAULT NULL,
  `RECHNUNGSBETRAG` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`BESTELLNR`),
  KEY `KUNDENNR` (`KUNDENNR`),
  CONSTRAINT `bestellung_ibfk_1` FOREIGN KEY (`KUNDENNR`) REFERENCES `kunde` (`KUNDENNR`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle krautundrueben.bestellung: ~12 rows (ungefähr)
DELETE FROM `bestellung`;
/*!40000 ALTER TABLE `bestellung` DISABLE KEYS */;
INSERT INTO `bestellung` (`BESTELLNR`, `KUNDENNR`, `BESTELLDATUM`, `RECHNUNGSBETRAG`) VALUES
	(1, 2001, '2020-07-01', 6.21),
	(2, 2002, '2020-07-08', 32.96),
	(3, 2003, '2020-08-01', 24.08),
	(4, 2004, '2020-08-02', 19.90),
	(5, 2005, '2020-08-02', 6.47),
	(6, 2006, '2020-08-10', 6.96),
	(7, 2007, '2020-08-10', 2.41),
	(8, 2008, '2020-08-10', 13.80),
	(9, 2009, '2020-08-10', 8.67),
	(10, 2007, '2020-08-15', 17.98),
	(11, 2005, '2020-08-12', 8.67),
	(12, 2003, '2020-08-13', 20.87);
/*!40000 ALTER TABLE `bestellung` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle krautundrueben.bestellungzutat
CREATE TABLE IF NOT EXISTS `bestellungzutat` (
  `BESTELLNR` int(11) NOT NULL,
  `ZUTATENNR` int(11) NOT NULL,
  `MENGE` int(11) DEFAULT NULL,
  PRIMARY KEY (`BESTELLNR`,`ZUTATENNR`),
  KEY `ZUTATENNR` (`ZUTATENNR`),
  CONSTRAINT `bestellungzutat_ibfk_1` FOREIGN KEY (`BESTELLNR`) REFERENCES `bestellung` (`BESTELLNR`),
  CONSTRAINT `bestellungzutat_ibfk_2` FOREIGN KEY (`ZUTATENNR`) REFERENCES `zutat` (`ZUTATENNR`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle krautundrueben.bestellungzutat: ~26 rows (ungefähr)
DELETE FROM `bestellungzutat`;
/*!40000 ALTER TABLE `bestellungzutat` DISABLE KEYS */;
INSERT INTO `bestellungzutat` (`BESTELLNR`, `ZUTATENNR`, `MENGE`) VALUES
	(1, 1001, 5),
	(1, 1002, 3),
	(1, 1004, 3),
	(1, 1006, 2),
	(2, 1003, 4),
	(2, 1005, 5),
	(2, 6408, 5),
	(2, 9001, 10),
	(3, 3001, 5),
	(3, 6300, 15),
	(4, 3003, 2),
	(4, 5001, 7),
	(5, 1001, 5),
	(5, 1002, 4),
	(5, 1004, 5),
	(6, 1010, 5),
	(7, 1009, 9),
	(8, 1008, 7),
	(8, 1012, 5),
	(9, 1007, 4),
	(9, 1012, 5),
	(10, 1011, 7),
	(10, 4001, 7),
	(11, 1012, 5),
	(11, 5001, 2),
	(12, 1010, 15);
/*!40000 ALTER TABLE `bestellungzutat` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle krautundrueben.kunde
CREATE TABLE IF NOT EXISTS `kunde` (
  `KUNDENNR` int(11) NOT NULL AUTO_INCREMENT,
  `NACHNAME` varchar(50) DEFAULT NULL,
  `VORNAME` varchar(50) DEFAULT NULL,
  `GEBURTSDATUM` date DEFAULT NULL,
  `STRASSE` varchar(50) DEFAULT NULL,
  `HAUSNR` varchar(6) DEFAULT NULL,
  `PLZ` varchar(5) DEFAULT NULL,
  `ORT` varchar(50) DEFAULT NULL,
  `TELEFON` varchar(25) DEFAULT NULL,
  `EMAIL` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`KUNDENNR`)
) ENGINE=InnoDB AUTO_INCREMENT=2011 DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle krautundrueben.kunde: ~10 rows (ungefähr)
DELETE FROM `kunde`;
/*!40000 ALTER TABLE `kunde` DISABLE KEYS */;
INSERT INTO `kunde` (`KUNDENNR`, `NACHNAME`, `VORNAME`, `GEBURTSDATUM`, `STRASSE`, `HAUSNR`, `PLZ`, `ORT`, `TELEFON`, `EMAIL`) VALUES
	(2001, 'Wellensteyn', 'Kira', '1990-05-05', 'Eppendorfer Landstrasse', '104', '20249', 'Hamburg', '040/443322', 'k.wellensteyn@yahoo.de'),
	(2002, 'Foede', 'Dorothea', '2000-03-24', 'Ohmstraße', '23', '22765', 'Hamburg', '040/543822', 'd.foede@web.de'),
	(2003, 'Leberer', 'Sigrid', '1989-09-21', 'Bilser Berg', '6', '20459', 'Hamburg', '0175/1234588', 'sigrid@leberer.de'),
	(2004, 'Soerensen', 'Hanna', '1974-04-03', 'Alter Teichweg', '95', '22049', 'Hamburg', '040/634578', 'h.soerensen@yahoo.de'),
	(2005, 'Schnitter', 'Marten', '1964-04-17', 'Stübels', '10', '22835', 'Barsbüttel', '0176/447587', 'schni_mart@gmail.com'),
	(2006, 'Maurer', 'Belinda', '1978-09-09', 'Grotelertwiete', '4a', '21075', 'Hamburg', '040/332189', 'belinda1978@yahoo.de'),
	(2007, 'Gessert', 'Armin', '1978-01-29', 'Küstersweg', '3', '21079', 'Hamburg', '040/67890', 'armin@gessert.de'),
	(2008, 'Haessig', 'Jean-Marc', '1982-08-30', 'Neugrabener Bahnhofstraße', '30', '21149', 'Hamburg', '0178-67013390', 'jm@haessig.de'),
	(2009, 'Urocki', 'Eric', '1999-12-04', 'Elbchaussee', '228', '22605', 'Hamburg', '0152-96701390', 'urocki@outlook.de'),
	(2010, 'Waldmann', 'Heinz', '2002-10-26', 'Spitaler Straße ', '2', '20097', 'Hamburg', '040/1234279', 'heinzi@ITECH.com');
/*!40000 ALTER TABLE `kunde` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle krautundrueben.lieferant
CREATE TABLE IF NOT EXISTS `lieferant` (
  `LIEFERANTENNR` int(11) NOT NULL,
  `LIEFERANTENNAME` varchar(50) DEFAULT NULL,
  `STRASSE` varchar(50) DEFAULT NULL,
  `HAUSNR` varchar(6) DEFAULT NULL,
  `PLZ` varchar(5) DEFAULT NULL,
  `ORT` varchar(50) DEFAULT NULL,
  `TELEFON` varchar(25) DEFAULT NULL,
  `EMAIL` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`LIEFERANTENNR`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle krautundrueben.lieferant: ~6 rows (ungefähr)
DELETE FROM `lieferant`;
/*!40000 ALTER TABLE `lieferant` DISABLE KEYS */;
INSERT INTO `lieferant` (`LIEFERANTENNR`, `LIEFERANTENNAME`, `STRASSE`, `HAUSNR`, `PLZ`, `ORT`, `TELEFON`, `EMAIL`) VALUES
	(101, 'Bio-Hof Müller', 'Dorfstraße', '74', '24354', 'Weseby', '04354-9080', 'mueller@biohof.de'),
	(102, 'Obst-Hof Altes Land', 'Westerjork ', '76', '21635', 'Jork', '04162-4523', 'info@biohof-altesland.de'),
	(103, 'Molkerei Henning', 'Molkereiwegkundekunde', '13', '19217', 'Dechow', '038873-8976', 'info@molkerei-henning.de'),
	(104, 'Markt Ardenau', 'Nnnerstraße', '13', '22113', 'Oststeinbek', '013678-9874', 'mardenau@markt.com'),
	(105, 'Fleischhof Klau', 'Metzgerstraße', '12', '21509', 'Glinde', '019383-4763', 'ilovefleisch@web.de'),
	(106, 'Mühle Kopsel', 'Mehler alee', '33', '21509', 'Glinde', '022445-2345', 'Mehlk@hof.de');
/*!40000 ALTER TABLE `lieferant` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle krautundrueben.rezept
CREATE TABLE IF NOT EXISTS `rezept` (
  `REZEPTNR` int(11) NOT NULL AUTO_INCREMENT,
  `REZEPTNAME` varchar(50) DEFAULT NULL,
  `ERNÄHRUNGSKATEGORIE` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`REZEPTNR`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle krautundrueben.rezept: ~4 rows (ungefähr)
DELETE FROM `rezept`;
/*!40000 ALTER TABLE `rezept` DISABLE KEYS */;
INSERT INTO `rezept` (`REZEPTNR`, `REZEPTNAME`, `ERNÄHRUNGSKATEGORIE`) VALUES
	(10, 'Lachslasagne', 'Fischgericht'),
	(11, 'Hotdog', 'Fastfood'),
	(12, 'Kürbisbrot', 'Vegetarisch'),
	(13, 'Lachs mit Kartoffeln', 'Fischgericht');
/*!40000 ALTER TABLE `rezept` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle krautundrueben.rezeptzutat
CREATE TABLE IF NOT EXISTS `rezeptzutat` (
  `REZEPTNR` int(11) NOT NULL,
  `ZUTATENNR` int(11) NOT NULL,
  `MENGE` int(11) DEFAULT NULL,
  KEY `ZUTATENNR` (`ZUTATENNR`),
  KEY `REZEPTNR` (`REZEPTNR`),
  CONSTRAINT `rezeptzutat_ibfk_1` FOREIGN KEY (`ZUTATENNR`) REFERENCES `zutat` (`ZUTATENNR`),
  CONSTRAINT `rezeptzutat_ibfk_2` FOREIGN KEY (`REZEPTNR`) REFERENCES `rezept` (`REZEPTNR`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle krautundrueben.rezeptzutat: ~23 rows (ungefähr)
DELETE FROM `rezeptzutat`;
/*!40000 ALTER TABLE `rezeptzutat` DISABLE KEYS */;
INSERT INTO `rezeptzutat` (`REZEPTNR`, `ZUTATENNR`, `MENGE`) VALUES
	(10, 2, 800),
	(10, 3, 300),
	(10, 4, 500),
	(11, 5001, 1),
	(12, 4001, 2),
	(10, 5, 150),
	(10, 6, 20),
	(10, 7, 0),
	(10, 8, 200),
	(10, 10, 75),
	(10, 11, 1),
	(10, 12, 1),
	(10, 3001, 250),
	(11, 13, 1),
	(12, 14, 1),
	(12, 6, 200),
	(12, 16, 50),
	(11, 15, 12),
	(13, 4, 500),
	(13, 1006, 600),
	(13, 1012, 40),
	(13, 11, 1),
	(13, 12, 30);
/*!40000 ALTER TABLE `rezeptzutat` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle krautundrueben.zutat
CREATE TABLE IF NOT EXISTS `zutat` (
  `ZUTATENNR` int(11) NOT NULL,
  `BEZEICHNUNG` varchar(50) DEFAULT NULL,
  `EINHEIT` varchar(25) DEFAULT NULL,
  `NETTOPREIS` decimal(10,2) DEFAULT NULL,
  `BESTAND` int(11) DEFAULT NULL,
  `LIEFERANT` int(11) DEFAULT NULL,
  `KALORIEN` decimal(20,6) DEFAULT NULL,
  `KOHLENHYDRATE` decimal(10,2) DEFAULT NULL,
  `PROTEIN` decimal(10,2) DEFAULT NULL,
  `ALLERGIEID` int(10) DEFAULT NULL,
  PRIMARY KEY (`ZUTATENNR`),
  KEY `LIEFERANT` (`LIEFERANT`),
  KEY `ALLERGIEID` (`ALLERGIEID`),
  CONSTRAINT `zutat_ibfk_1` FOREIGN KEY (`LIEFERANT`) REFERENCES `lieferant` (`LIEFERANTENNR`),
  CONSTRAINT `zutat_ibfk_2` FOREIGN KEY (`ALLERGIEID`) REFERENCES `allergene` (`ALLERGIEID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle krautundrueben.zutat: ~36 rows (ungefähr)
DELETE FROM `zutat`;
/*!40000 ALTER TABLE `zutat` DISABLE KEYS */;
INSERT INTO `zutat` (`ZUTATENNR`, `BEZEICHNUNG`, `EINHEIT`, `NETTOPREIS`, `BESTAND`, `LIEFERANT`, `KALORIEN`, `KOHLENHYDRATE`, `PROTEIN`, `ALLERGIEID`) VALUES
	(2, 'Spinat', 'Gramm', 0.70, 2000, 101, 0.120000, 1.00, 2.00, NULL),
	(3, 'Lasagneplatten', 'Gramm', 1.30, 1000, 104, 3.620000, 71.20, 12.30, 1),
	(4, 'Lachs', 'Gramm', 1.30, 20000, 105, 1.800000, 0.40, 19.90, 6),
	(5, 'Käse', 'Gramm', 0.90, 60000, 103, 2.340000, 2.50, 10.00, 1),
	(6, 'Mehl', 'Gramm', 1.00, 90000, 106, 3.420000, 70.40, 10.00, 3),
	(7, 'Brühe', 'Milliliter', 0.50, 4, 104, 4.000000, 1.00, 0.90, NULL),
	(8, 'Sahne', 'Gramm', 0.34, 2000, 103, 0.100000, 3.00, 0.30, 1),
	(10, 'Parmesan-käse', 'Gramm', 1.00, 4000, 103, 2.320000, 2.34, 1.80, 1),
	(11, 'Zitrone', 'Stück', 0.60, 40, 102, 28.000000, 24.00, 1.10, NULL),
	(12, 'Pfeffer', 'Gramm', 2.90, 1000, 104, 0.200000, 1.00, 1.96, NULL),
	(13, 'Hotdog-brot', 'Stück', 0.21, 250, 104, 204.000000, 0.50, 2.02, 3),
	(14, 'Kürbis', 'Stück', 1.20, 30, 101, 20.000000, 1.00, 12.20, NULL),
	(15, 'Ketchup', 'Gramm', 1.00, 1000, 104, 2.500000, 1.00, 2.00, NULL),
	(16, 'Nüsse', 'Gramm', 2.10, 3000, 104, 0.300000, 16.00, 2.40, 2),
	(1001, 'Zucchini', 'Stück', 0.89, 100, 101, 19.000000, 2.00, 1.60, NULL),
	(1002, 'Zwiebel', 'Stück', 0.15, 50, 101, 28.000000, 4.90, 1.20, NULL),
	(1003, 'Tomate', 'Stück', 0.45, 50, 101, 18.000000, 2.60, 1.00, NULL),
	(1004, 'Schalotte', 'Stück', 0.20, 500, 101, 25.000000, 3.30, 1.50, NULL),
	(1005, 'Karotte', 'Stück', 0.30, 500, 101, 41.000000, 10.00, 0.90, NULL),
	(1006, 'Kartoffel', 'Stück', 0.15, 1500, 101, 71.000000, 14.60, 2.00, NULL),
	(1007, 'Rucola', 'Bund', 0.90, 10, 101, 27.000000, 2.10, 2.60, NULL),
	(1008, 'Lauch', 'Stück', 1.20, 35, 101, 29.000000, 3.30, 2.10, NULL),
	(1009, 'Knoblauch', 'Stück', 0.25, 250, 101, 141.000000, 28.40, 6.10, NULL),
	(1010, 'Basilikum', 'Bund', 1.30, 10, 101, 41.000000, 5.10, 3.10, NULL),
	(1011, 'Süßkartoffel', 'Stück', 2.00, 200, 101, 86.000000, 20.00, 1.60, NULL),
	(1012, 'Schnittlauch', 'Bund', 0.90, 10, 101, 28.000000, 1.00, 3.00, NULL),
	(2001, 'Apfel', 'Stück', 1.20, 750, 102, 54.000000, 14.40, 0.30, 4),
	(3001, 'Vollmilch. 3.5%', 'Milliliter', 1.50, 50, 103, 65.000000, 4.70, 3.40, 1),
	(3002, 'Mozzarella', 'Packung', 3.50, 20, 103, 241.000000, 1.00, 18.10, NULL),
	(3003, 'Butter', 'Gramm', 3.00, 50, 103, 7.410000, 0.60, 0.70, 1),
	(4001, 'Ei', 'Stück', 0.40, 300, 102, 137.000000, 1.50, 11.90, 5),
	(5001, 'Wiener Würstchen', 'Paar', 1.80, 40, 101, 331.000000, 1.20, 9.90, NULL),
	(6300, 'Kichererbsen', 'Dose', 1.00, 400, 103, 150.000000, 21.20, 9.00, NULL),
	(6408, 'Couscous', 'Packung', 1.90, 15, 102, 351.000000, 67.00, 12.00, NULL),
	(7043, 'Gemüsebrühe', 'Würfel', 0.20, 4000, 101, 1.000000, 0.50, 0.50, NULL),
	(9001, 'Tofu-Würstchen', 'Stück', 1.80, 20, 103, 252.000000, 7.00, 17.00, NULL);
/*!40000 ALTER TABLE `zutat` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
