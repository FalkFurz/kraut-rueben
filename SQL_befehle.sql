/* SQL-Abfrage zur Auswahl aller Zutaten eines Rezeptes nach Rezeptname */


SELECT rezept.REZEPTNAME, zutat.BEZEICHNUNG AS ZUTATEN, rezeptzutat.MENGE, zutat.EINHEIT
FROM rezeptzutat
INNER JOIN zutat ON rezeptzutat.ZUTATENNR = zutat.ZUTATENNR
INNER JOIN rezept ON rezeptzutat.REZEPTNR = rezept.REZEPTNR

WHERE rezept.REZEPTNAME =/*eingabr des gesuchten Rezeptes -------->*/ 'Kartoffen'




/*-----------------------------------------------------------------------------------------*/
/* SQL-Abfrage zur Auswahl aller Rezepte einer bestimmten Ernährungskategorie */


SELECT rezept.REZEPTNAME, zutat.BEZEICHNUNG AS ZUTATEN, rezeptzutat.MENGE, zutat.EINHEIT, rezept.`ERNÄHRUNGSKATEGORIE`
FROM rezeptzutat
INNER JOIN zutat ON rezeptzutat.ZUTATENNR = zutat.ZUTATENNR
INNER JOIN rezept ON rezeptzutat.REZEPTNR = rezept.REZEPTNR

WHERE rezept.`ERNÄHRUNGSKATEGORIE` = /*eingabe der gesuchten Kategorie--->*/'ulu' 

/*---------------------------------------------------------------------------------------------------------*/

/* SQL-Abfrage zur Auswahl aller Rezepte, die eine gewisse Zutat enthalten */


SELECT rezept.REZEPTNAME, zutat.BEZEICHNUNG AS ZUTATEN, rezeptzutat.MENGE, zutat.EINHEIT
FROM rezeptzutat
INNER JOIN zutat ON rezeptzutat.ZUTATENNR = zutat.ZUTATENNR
INNER JOIN rezept ON rezeptzutat.REZEPTNR = rezept.REZEPTNR

WHERE zutat.BEZEICHNUNG = 'Butter'

/*---------------------------------------------------------------------------------------------------------------------*/
/* SQL-Abfrage zur Auswahl des Mittelwert des Nährwertes eine Kunden*/

SELECT kunde.NACHNAME, kunde.VORNAME, 
 sum(bestellungzutat.menge * zutat.KALORIEN) / COUNT(bestellungzutat.BESTELLNR) AS 'Durchschnitt Kalorien',
 sum(bestellungzutat.menge * zutat.KOHLENHYDRATE) / COUNT(bestellungzutat.BESTELLNR) AS 'Durchschnitt Kohlenhydrate',
 sum(bestellungzutat.menge * zutat.PROTEIN) / COUNT(bestellungzutat.BESTELLNR) AS 'Durchschnitt Protein'


FROM zutat
INNER JOIN bestellungzutat ON zutat.ZUTATENNR = bestellungzutat.ZUTATENNR 
INNER JOIN bestellung ON bestellungzutat.BESTELLNR = bestellung.BESTELLNR
inner JOIN kunde ON bestellung.KUNDENNR = kunde.KUNDENNR

WHERE kunde.NACHNAME =/* Hier gesuchten Kunden eingeben--->*/ 'g'

/*----------------------------------------------------------------------------------------------------------------*/
/* SQL-Abfrage zur Auswahl aller Zutaten, die bisher keinem Rezept zugeordnet sind */

SELECT zutat.BEZEICHNUNG

FROM zutat 
LEFT JOIN rezeptzutat ON zutat.ZUTATENNR = rezeptzutat.ZUTATENNR

WHERE rezeptzutat.REZEPTNR IS NULL

/*-------------------------------------------------------------------------------------------------------------*/
/* SQL-Abfrage zur suchen von allergene im Rezept */
SELECT zutat.BEZEICHNUNG, rezept.REZEPTNAME

FROM allergene 

INNER JOIN zutat ON allergene.ALLERGIEID = zutat.ALLERGIEID
INNER JOIN rezeptzutat ON zutat.ZUTATENNR = rezeptzutat.ZUTATENNR
INNER JOIN rezept ON rezeptzutat.REZEPTNR = rezept.REZEPTNR

WHERE allergene.ALLLERGIENAME = "Apfel"

/*------------------------------------------------------------------------------------------------------------------*/
/* SQL-Abfrage zum filtern der Allergene */

SELECT * FROM rezept WHERE rezept.REZEPTNR NOT IN (

SELECT rezept.REZEPTNR

FROM allergene 

INNER JOIN zutat ON allergene.ALLERGIEID = zutat.ALLERGIEID
INNER JOIN rezeptzutat ON zutat.ZUTATENNR = rezeptzutat.ZUTATENNR
INNER JOIN rezept ON rezeptzutat.REZEPTNR = rezept.REZEPTNR

WHERE allergene.ALLLERGIENAME = "Apfel")
/*-----------------------------------------------------------------------------------------------------*/
/* Gesammtkalorien eines Rezeptes Anzeigen */ 
SELECT rezept.REZEPTNAME, SUM(zutat.KALORIEN * rezeptzutat.MENGE) AS "gesammt Kalorien"

FROM zutat
INNER JOIN rezeptzutat ON zutat.ZUTATENNR = rezeptzutat.ZUTATENNR
INNER JOIN rezept ON rezeptzutat.REZEPTNR = rezept.REZEPTNR

WHERE rezept.REZEPTNAME = "gogo"
/*-----------------------------------------------------------------------------------------------------------------*/
/* SQL-Abfrage zur Filterung der Maximalen KalorienMenge eines Rezeptes*/
SELECT r2.a , r2.REZEPTNAME FROM (SELECT sum(r1.MENGE * r1.KALORIEN) AS a, r1.REZEPTNAME From (SELECT rezept.REZEPTNAME, zutat.BEZEICHNUNG AS ZUTATEN, rezeptzutat.MENGE, zutat.EINHEIT, zutat.KALORIEN 
FROM rezeptzutat
INNER JOIN zutat ON rezeptzutat.ZUTATENNR = zutat.ZUTATENNR
INNER JOIN rezept ON rezeptzutat.REZEPTNR = rezept.REZEPTNR

)r1 


GROUP BY r1.REZEPTNAME)r2
WHERE a <= /*Hier gewünschte Kalorienmenge eintragen ---->*/145
/*-----------------------------------------------------------------------------------------------------------------*/
/* SQL-Abfrage zur Filterung von Bestellungen anhand des Datumes*/
SELECT bestellung.BESTELLDATUM, bestellung.BESTELLNR ,kunde.VORNAME, kunde.NACHNAME

FROM bestellung
INNER JOIN kunde ON bestellung.KUNDENNR = kunde.KUNDENNR

WHERE bestellung.BESTELLDATUM = /* Datum ohne Bindestrich oder Leerzeichen*/20200701
/*-------------------------------------------------------------------------------------------------------------------*/ 